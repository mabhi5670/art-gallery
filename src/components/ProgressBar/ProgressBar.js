import React from 'react';
import ImgDatabase from '../ImgDatabase/ImgDatabase';

const Bar = ({ file, setFile }) => {
  const { url, progress } = ImgDatabase(file);
  console.log('url>>><<<<<<<<', progress, url);
  return (
    <div className='progress-bar' style={{ width: progress + '%' }}>
      progress
    </div>
  );
};

export default Bar;
