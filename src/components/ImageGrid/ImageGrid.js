import React from 'react';
import UserFirestore from '../UserFirestore/UserFirestore';

const ImageGrid = ({ setSelectedImg }) => {
  const { docs } = UserFirestore('images');
  console.log(docs);
  return (
    <div className='img-grid'>
      {docs &&
        docs.map((doc) => (
          <div className='img-wrap' onClick={() => setSelectedImg(doc.url)}>
            <img src={doc.url} alt='uploaded pic' />
          </div>
        ))}
    </div>
  );
};
export default ImageGrid;
