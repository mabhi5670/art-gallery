// import Bar from 'progress';
// import React, { useState } from 'react';

// const UploadForm = () => {
//   const [file, setFile] = useState();

//   const [error, setError] = useState('');

//   // const SetAlert = (msg) => {
//   //   setAlert({ msg });
//   //   setTimeout(() => setAlert(null), 5000);
//   // };
//   //  const [alert, setAlert] = useState('');
//   // const [file2, setFile2] = useState(null);
//   // const [error, setError] = useState(null);
//   const types = ['image/jpeg', 'image/png'];
//   const changeHandler = (event) => {
//     event.stopPropagation();
//     console.log('>>>>event', event);
//     const selected = event.target.files[0];
//     // const arr = selected.slice(0, 6);
//     // console.log(arr, '>>>');
//     // console.log(selected.type, '>>>');
//     // console.log('>>>>selected', types.includes(selected.type));

//     if (selected && types.includes(selected.type)) {
//       setFile(selected);
//       // setFile2(event.target.value);
//       setError('');
//       console.log(setFile, selected, 'hfafsfxas...');
//     } else {
//       setFile(null);
//       // setFile2('');
//       setError('Please select an image file (png or jpeg)');
//     }
//   };
//   // const onUpload = (event) => event.target.files[0];
//   // console.log(onUpload, 'gjagsdj>>><<<<');
//   return (
//     <form>
//       <label>
//         <input type='file' onChange={changeHandler} />
//         {/* {alert} */}
//         <span>+</span>
//       </label>

//       <div className='output'>
//         {error && <div className='error'>{error}</div>}
//         {file && <div>{file.name}</div>}
//         {file && <Bar file={file} setFile={setFile} />}
//       </div>
//     </form>
//   );
// };

// export default UploadForm;

import React, { useState } from 'react';
import ProgressBar from '../ProgressBar/ProgressBar';
const UploadForm = () => {
  const [file, setFile] = useState(null);
  const [error, setError] = useState(null);

  const types = ['image/png', 'image/jpeg'];

  const handleChange = (e) => {
    let selected = e.target.files[0];

    if (selected && types.includes(selected.type)) {
      setFile(selected);
      setError('');
    } else {
      setFile(null);
      setError('Please select an image file (png or jpg)');
    }
  };

  return (
    <form>
      <label>
        <input type='file' onChange={handleChange} />
        <span>+</span>
      </label>
      <div className='output'>
        {error && <div className='error'>{error}</div>}
        {file && <div>{file.name}</div>}
        {file && <ProgressBar file={file} setFile={setFile} />}
      </div>
    </form>
  );
};

export default UploadForm;
