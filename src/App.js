import React, { useState } from 'react';
import ImageGrid from './components/ImageGrid/ImageGrid.js';
import Modal from './components/Modal/Modal';
import Title from './components/Title/Title';
import UploadForm from './components/UploadForm/UploadForm';

const App = () => {
  const [selectedImg, setSelectedImg] = useState(null);
  return (
    <div className='App'>
      <Title />
      <UploadForm />
      <ImageGrid setSelectedImg={setSelectedImg} />
      {selectedImg && (
        <Modal selectedImg={selectedImg} setSelectedImg={setSelectedImg} />
      )}
    </div>
  );
};

export default App;
