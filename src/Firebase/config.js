import firebase from 'firebase/compat/app';
import 'firebase/compat/storage';
import 'firebase/compat/firestore';

// Import the functions you need from the SDKs you need
//import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.2.0/firebase-app.js';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyBa9Rmo2USupDvSOLX2CpJ5P-0xX_lYFVs',
  authDomain: 'art-gallery-328fd.firebaseapp.com',
  projectId: 'art-gallery-328fd',
  storageBucket: 'art-gallery-328fd.appspot.com',
  messagingSenderId: '481384434082',
  appId: '1:481384434082:web:10b6992034ea5dfa578afb',
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const projectStorage = firebase.storage();
const projectFirestore = firebase.firestore();
const timestamp = firebase.firestore.FieldValue.serverTimestamp;
export { projectStorage, projectFirestore, timestamp };
